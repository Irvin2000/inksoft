<?php

class Aprendices extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('aprendiz');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('aprendices/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['aprendices']=$this->aprendiz->obtenerTodos();
    $this->load->view('header');
    $this->load->view('aprendices/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoAprendiz=array(
      "nombre_apre_cef"=>$this->input->post('nombre_apre_cef'),
      "apellido_apre_cef"=>$this->input->post('apellido_apre_cef'),
      "cedula_apre_cef"=>$this->input->post('cedula_apre_cef'),
      "telefono_apre_cef"=>$this->input->post('telefono_apre_cef'),
      "correo_apre_cef"=>$this->input->post('correo_apre_cef')
    );
    if($this->aprendiz->insertar($datosNuevoAprendiz)){
    $this->session->set_flashdata("confirmacion","Aprendiz guardado exitosamente");
  }else {
    $this->session->set_flashdata("error","Error al guardar intente de nuevo");
  }
  redirect('aprendices/index');
} //cierre de la funcion guardar
  //funcion para Eliminar aprendices
  //metodo get
  public function eliminar($id_apre_cef){
    if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
      $this->session->set_flashdata("error","No tiene permisos para eliminar");
      redirect("aprendices/index");
    }
   if ($this->aprendiz->borrar($id_apre_cef)) { //invocando al modelo
     $this->session->set_flashdata("confirmacion","Aprendiz eliminar exitosamente");

   } else {
     $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
   }

        redirect('aprendices/index');

  }
  //function renderizar vista editar con el aprendiz
  public function editar($id_apre_cef){
    $data["aprendizEditar"]=$this->aprendiz->obtenerPorId($id_apre_cef);
    $this->load->view('header');
    $this->load->view('aprendices/editar',$data);
    $this->load->view('footer');
  }
  public function procesarActualizacion(){
    $datosEditados=array(
      "nombre_apre_cef"=>$this->input->post('nombre_apre_cef'),
      "apellido_apre_cef"=>$this->input->post('apellido_apre_cef'),
      "cedula_apre_cef"=>$this->input->post('cedula_apre_cef'),
      "telefono_apre_cef"=>$this->input->post('telefono_apre_cef'),
      "correo_apre_cef"=>$this->input->post('correo_apre_cef')
    );
    $id_apre_cef=$this->input->post("id_apre_cef");
    if ($this->aprendiz->actualizar($id_apre_cef,$datosEditados)) {
      $this->session->set_flashdata("confirmacion","Aprendiz actualizado exitosamente");

    } else {
      $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
    }
    redirect("aprendices/index");

  }

}//NO borrar el cierre de la clase


 ?>
