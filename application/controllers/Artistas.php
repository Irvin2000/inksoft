<?php

class Artistas extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('artista');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('artistas/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['artistas']=$this->artista->obtenerTodos();
    $this->load->view('header');
    $this->load->view('artistas/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoArtista=array(
      "nombre_artista_cef"=>$this->input->post('nombre_artista_cef'),
      "apellido_artista_cef"=>$this->input->post('apellido_artista_cef'),
      "cedula_artista_cef"=>$this->input->post('cedula_artista_cef'),
      "telefono_artista_cef"=>$this->input->post('telefono_artista_cef'),
      "correo_artista_cef"=>$this->input->post('correo_artista_cef')
    );
    if($this->artista->insertar($datosNuevoArtista)){
    $this->session->set_flashdata("confirmacion","Artista guardado exitosamente");
  }else {
    $this->session->set_flashdata("error","Error al guardar intente de nuevo");
  }
  redirect('artistas/index');
} //cierre de la funcion guardar
  //funcion para Eliminar Artistas
  //metodo get
  public function eliminar($id_artista_cef){
    if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
      $this->session->set_flashdata("error","No tiene permisos para eliminar");
      redirect("artistas/index");
    }
   if ($this->artista->borrar($id_artista_cef)) { //invocando al modelo
     $this->session->set_flashdata("confirmacion","Artista eliminado exitosamente");

   } else {
     $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
   }

        redirect('artistas/index');

  }
  //function renderizar vista editar con el instructor
  public function editar($id_artista_cef){
    $data["artistaEditar"]=$this->artista->obtenerPorId($id_artista_cef);
    $this->load->view('header');
    $this->load->view('artistas/editar',$data);
    $this->load->view('footer');
  }
  public function procesarActualizacion(){
    $datosEditados=array(
      "nombre_artista_cef"=>$this->input->post('nombre_artista_cef'),
      "apellido_artista_cef"=>$this->input->post('apellido_artista_cef'),
      "cedula_artista_cef"=>$this->input->post('cedula_artista_cef'),
      "telefono_artista_cef"=>$this->input->post('telefono_artista_cef'),
      "correo_artista_cef"=>$this->input->post('correo_artista_cef')
    );
    $id_artista_cef=$this->input->post("id_artista_cef");
    if ($this->artista->actualizar($id_artista_cef,$datosEditados)) {
      $this->session->set_flashdata("confirmacion","Artista actualizado exitosamente");

    } else {
      $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
    }
    redirect("artistas/index");

  }

}//NO borrar el cierre de la clase


 ?>
