<?php

class Citas extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('cita');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('citas/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['citas']=$this->cita->obtenerTodos();
    $this->load->view('header');
    $this->load->view('citas/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevaCita=array(
      "fecha_cita_cef"=>$this->input->post('fecha_cita_cef'),
      "hora_cita_cef"=>$this->input->post('hora_cita_cef'),
      "detalle_cita_cef"=>$this->input->post('detalle_cita_cef'),
      "costo_cita_cef"=>$this->input->post('costo_cita_cef'),
    );
    if($this->cita->insertar($datosNuevaCita)){
      $this->session->set_flashdata("confirmacion","Cita guardado exitosamente");
    }else {
      $this->session->set_flashdata("error","Error al guardar intente de nuevo");
    }
    redirect('citas/index');
  }
  //funcion para Eliminar citas
  //metodo get
  public function eliminar($id_cita_cef){
    if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
      $this->session->set_flashdata("error","No tiene permisos para eliminar");
      redirect("citas/index");
    }
   if ($this->cita->borrar($id_cita_cef)) { //invocando al modelo
     $this->session->set_flashdata("confirmacion","Cita eliminado exitosamente");

   } else {
     $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
   }

        redirect('citas/index');

  }
  //function renderizar vista editar con el Cita
  public function editar($id_cita_cef){
    $data["citaEditar"]=$this->cita->obtenerPorId($id_cita_cef);
    $this->load->view('header');
    $this->load->view('citas/editar',$data);
    $this->load->view('footer');
  }
  public function procesarActualizacion(){
    $datosEditados=array(
      "fecha_cita_cef"=>$this->input->post('fecha_cita_cef'),
      "hora_cita_cef"=>$this->input->post('hora_cita_cef'),
      "detalle_cita_cef"=>$this->input->post('detalle_cita_cef'),
      "costo_cita_cef"=>$this->input->post('costo_cita_cef')
    );
    $id_cita_cef=$this->input->post('id_cita_cef');
    if ($this->cita->actualizar($id_cita_cef,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Cita actualizado exitosamente");

    } else {
      $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
    }
     redirect("citas/index");
  }
}//NO borrar el cierre de la clase


 ?>
