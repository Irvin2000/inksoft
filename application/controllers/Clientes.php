<?php

class Clientes extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('cliente');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('clientes/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['clientes']=$this->cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('clientes/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoCliente=array(
      "nombre_cli_cef"=>$this->input->post('nombre_cli_cef'),
      "apellido_cli_cef"=>$this->input->post('apellido_cli_cef'),
      "telefono_cli_cef"=>$this->input->post('telefono_cli_cef'),
      "correo_cli_cef"=>$this->input->post('correo_cli_cef'),
    );
    if($this->cliente->insertar($datosNuevoCliente)){
      $this->session->set_flashdata("confirmacion","Cliente guardado exitosamente");
    }else {
      $this->session->set_flashdata("error","Error al guardar intente de nuevo");
    }
    redirect('clientes/index');
  }
  //funcion para Eliminar Instructores
  //metodo get
  public function eliminar($id_cli_cef){
    if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
      $this->session->set_flashdata("error","No tiene permisos para eliminar");
      redirect("clientes/index");
    }
   if ($this->artista->borrar($id_cli_cef)) { //invocando al modelo
     $this->session->set_flashdata("confirmacion","Cliente eliminado exitosamente");

   } else {
     $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
   }

        redirect('clientes/index');

  }
  //function renderizar vista editar con el instructor
  public function editar($id_cli_cef){
    $data["clienteEditar"]=$this->cliente->obtenerPorId($id_cli_cef);
    $this->load->view('header');
    $this->load->view('clientes/editar',$data);
    $this->load->view('footer');
  }
public function procesarActualizacion(){
  $datosEditados=array(
    "nombre_cli_cef"=>$this->input->post('nombre_cli_cef'),
    "apellido_cli_cef"=>$this->input->post('apellido_cli_cef'),
    "telefono_cli_cef"=>$this->input->post('telefono_cli_cef'),
    "correo_cli_cef"=>$this->input->post('correo_cli_cef'),
  );
  $id_cliente_cef=$this->input->post("id_cliente_cef");
  if ($this->cliente->actualizar($id_cliente_cef,$datosEditados)){
    $this->session->set_flashdata("confirmacion","Cliente actualizado exitosamente");

  } else {
    $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
  }
   redirect("clientes/index");
}
}//NO borrar el cierre de la clase


 ?>
