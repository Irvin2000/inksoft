<?php

class Disenos extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('diseno');
  }
  //Renderizacion de la vista que
  //muestra los disenos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('disenos/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['disenos']=$this->diseno->obtenerTodos();
    $this->load->view('header');
    $this->load->view('disenos/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoDiseno=array(
      "nombre_diseno_cef"=>$this->input->post('nombre_diseno_cef'),
      "ancho_diseno_cef"=>$this->input->post('ancho_diseno_cef'),
      "largo_diseno_cef"=>$this->input->post('largo_diseno_cef'),
      "costo_diseno_cef"=>$this->input->post('costo_diseno_cef'),
      "estilo_diseno_cef"=>$this->input->post('estilo_diseno_cef')
    );
    if($this->diseno->insertar($datosNuevoDiseno)){
      $this->session->set_flashdata("confirmacion","Diseño guardado exitosamente");
    }else {
      $this->session->set_flashdata("error","Error al guardar intente de nuevo");
    }
    redirect('disenos/index');
  }
  //funcion para Eliminar disenos
  //metodo get
  public function eliminar($id_diseno_cef){
    if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
      $this->session->set_flashdata("error","No tiene permisos para eliminar");
      redirect("disenos/index");
    }
   if ($this->diseno->borrar($id_diseno_cef)) { //invocando al modelo
     $this->session->set_flashdata("confirmacion","Diseño eliminado exitosamente");

   } else {
     $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
   }

        redirect('disenos/index');

  }
  //function renderizar vista editar con el instructor
  public function editar($id_diseno_cef){
    $data["disenoEditar"]=$this->diseno->obtenerPorId($id_diseno_cef);
    $this->load->view('header');
    $this->load->view('disenos/editar',$data);
    $this->load->view('footer');
  }
  public function procesarActualizacion(){
    $datosEditados=array(
      "nombre_diseno_cef"=>$this->input->post('nombre_diseno_cef'),
      "ancho_diseno_cef"=>$this->input->post('ancho_diseno_cef'),
      "largo_diseno_cef"=>$this->input->post('largo_diseno_cef'),
      "costo_diseno_cef"=>$this->input->post('costo_diseno_cef'),
      "estilo_diseno_cef"=>$this->input->post('estilo_diseno_cef')
    );
    $id_diseno_cef=$this->input->post("id_diseno_cef");
    if ($this->diseno->actualizar($id_diseno_cef,$datosEditados)) {
      $this->session->set_flashdata("confirmacion","Diseño actualizado exitosamente");

    } else {
      $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
    }
     redirect("disenos/index");
  }


}//NO borrar el cierre de la clase


 ?>
