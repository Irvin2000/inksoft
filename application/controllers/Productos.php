<?php

class Productos extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('producto');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('productos/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['productos']=$this->producto->obtenerTodos();
    $this->load->view('header');
    $this->load->view('productos/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoproducto=array(
      "nombre_pro_cef"=>$this->input->post('nombre_pro_cef'),
      "detalle_pro_cef"=>$this->input->post('detalle_pro_cef'),
      "precio_pro_cef"=>$this->input->post('precio_pro_cef')
        );
    if($this->producto->insertar($datosNuevoproducto)){
    $this->session->set_flashdata("confirmacion","producto guardado exitosamente");
  }else {
    $this->session->set_flashdata("error","Error al guardar intente de nuevo");
  }
  redirect('productos/index');
} //cierre de la funcion guardar
  //funcion para Eliminar productos
  //metodo get
  public function eliminar($id_pro_cef){
    if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
      $this->session->set_flashdata("error","No tiene permisos para eliminar");
      redirect("productos/index");
    }
   if ($this->producto->borrar($id_pro_cef)) { //invocando al modelo
     $this->session->set_flashdata("confirmacion","Producto eliminado exitosamente");

   } else {
     $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
   }

        redirect('productos/index');

  }
  //function renderizar vista editar con el instructor
  public function editar($id_pro_cef){
    $data["productoEditar"]=$this->producto->obtenerPorId($id_pro_cef);
    $this->load->view('header');
    $this->load->view('productos/editar',$data);
    $this->load->view('footer');
  }
  public function procesarActualizacion(){
    $datosEditados=array(
      "nombre_pro_cef"=>$this->input->post('nombre_pro_cef'),
      "detalle_pro_cef"=>$this->input->post('detalle_pro_cef'),
      "precio_pro_cef"=>$this->input->post('precio_pro_cef')
        );
    $id_pro_cef=$this->input->post("id_pro_cef");
    if ($this->producto->actualizar($id_pro_cef,$datosEditados)) {
      $this->session->set_flashdata("confirmacion","Producto actualizada exitosamente");

    } else {
      $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
    }
    redirect("productos/index");

  }

}//NO borrar el cierre de la clase


 ?>
