<?php

class Promociones extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('promocion');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('promociones/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['promociones']=$this->promocion->obtenerTodos();
    $this->load->view('header');
    $this->load->view('promociones/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoPromocion=array(
      "tipo_pro_cef"=>$this->input->post('tipo_pro_cef'),
      "costo_pro_cef"=>$this->input->post('costo_pro_cef'),
      "fecha_inicio_pro_cef"=>$this->input->post('fecha_inicio_pro_cef'),
      "fecha_final_pro_cef"=>$this->input->post('fecha_final_pro_cef')
        );
    if($this->promocion->insertar($datosNuevoPromocion)){
    $this->session->set_flashdata("confirmacion","Promocion guardado exitosamente");
  }else {
    $this->session->set_flashdata("error","Error al guardar intente de nuevo");
  }
  redirect('promociones/index');
} //cierre de la funcion guardar
  //funcion para Eliminar promociones
  //metodo get
  public function eliminar($id_pro_cef){
    if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
      $this->session->set_flashdata("error","No tiene permisos para eliminar");
      redirect("promociones/index");
    }
   if ($this->artista->borrar($id_pro_cef)) { //invocando al modelo
     $this->session->set_flashdata("confirmacion","Promocion eliminada exitosamente");

   } else {
     $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
   }

        redirect('promociones/index');

  }
  //function renderizar vista editar con el instructor
  public function editar($id_pro_cef){
    $data["promocionEditar"]=$this->promocion->obtenerPorId($id_pro_cef);
    $this->load->view('header');
    $this->load->view('promociones/editar',$data);
    $this->load->view('footer');
  }
  public function procesarActualizacion(){
    $datosEditados=array(
      "tipo_pro_cef"=>$this->input->post('tipo_pro_cef'),
      "costo_pro_cef"=>$this->input->post('costo_pro_cef'),
      "fecha_inicio_pro_cef"=>$this->input->post('fecha_inicio_pro_cef'),
      "fecha_final_pro_cef"=>$this->input->post('fecha_final_pro_cef')
        );
    $id_pro_cef=$this->input->post("id_pro_cef");
    if ($this->promocion->actualizar($id_pro_cef,$datosEditados)) {
      $this->session->set_flashdata("confirmacion","Promoción actualizada exitosamente");

    } else {
      $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
    }
    redirect("promociones/index");

  }

}//NO borrar el cierre de la clase


 ?>
