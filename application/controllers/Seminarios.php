<?php

class Seminarios extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('seminario');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('seminarios/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['seminarios']=$this->seminario->obtenerTodos();
    $this->load->view('header');
    $this->load->view('seminarios/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoSeminario=array(
      "nombre_semi_cef"=>$this->input->post('nombre_semi_cef'),
      "tema_semi_cef"=>$this->input->post('tema_semi_cef'),
      "detalle_semi_cef"=>$this->input->post('detalle_semi_cef'),
      "horas_semi_cef"=>$this->input->post('horas_semi_cef'),
      "fecha_semi_cef"=>$this->input->post('fecha_semi_cef')
        );
    if($this->seminario->insertar($datosNuevoSeminario)){
    $this->session->set_flashdata("confirmacion","Seminario guardado exitosamente");
  }else {
    $this->session->set_flashdata("error","Error al guardar intente de nuevo");
  }
  redirect('seminarios/index');
} //cierre de la funcion guardar
  //funcion para Eliminar seminarios
  //metodo get
  public function eliminar($id_semi_cef){
    if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
      $this->session->set_flashdata("error","No tiene permisos para eliminar");
      redirect("seminarios/index");
    }
   if ($this->seminario->borrar($id_semi_cef)) { //invocando al modelo
     $this->session->set_flashdata("confirmacion","seminario eliminado exitosamente");

   } else {
     $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
   }

        redirect('seminarios/index');

  }
  //function renderizar vista editar con el instructor
  public function editar($id_semi_cef){
    $data["seminarioEditar"]=$this->seminario->obtenerPorId($id_semi_cef);
    $this->load->view('header');
    $this->load->view('seminarios/editar',$data);
    $this->load->view('footer');
  }
  public function procesarActualizacion(){
    $datosEditados=array(
      "nombre_semi_cef"=>$this->input->post('nombre_semi_cef'),
      "tema_semi_cef"=>$this->input->post('tema_semi_cef'),
      "detalle_semi_cef"=>$this->input->post('detalle_semi_cef'),
      "horas_semi_cef"=>$this->input->post('horas_semi_cef'),
      "fecha_semi_cef"=>$this->input->post('fecha_semi_cef')
        );
    $id_semi_cef=$this->input->post("id_semi_cef");
    if ($this->seminario->actualizar($id_semi_cef,$datosEditados)) {
      $this->session->set_flashdata("confirmacion","Seminario actualizada exitosamente");
    } else {
      $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
    }
    redirect("seminarios/index");

  }

}//NO borrar el cierre de la clase


 ?>
