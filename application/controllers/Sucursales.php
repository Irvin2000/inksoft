<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursales extends CI_Controller {

  public function __construct(){
  parent::__construct();
  $this->load->model("sucursal");
}

	public function index()
	{
    $data["listadoSucursales"]=
    $this->sucursal->obtenerTodos();
		$this->load->view('header');
		$this->load->view('sucursales/index',$data);
		$this->load->view('footer');
	}

  public function nuevo()
  {
    $this->load->view('header');
    $this->load->view('sucursales/nuevo');
    $this->load->view('footer');
  }

  public function guardarSucursales(){
		$datosNuevoSucursal=array(
      "id_suc"=>$this->input->post('id_suc'),
      "nombre_suc"=>$this->input->post('nombre_suc'),
			"ciudad_suc"=>$this->input->post('ciudad_suc'),
      "latitud_suc"=>$this->input->post('latitud_suc'),
      "longitud_suc"=>$this->input->post('longitud_suc'),
      "latitud_suc"=>$this->input->post('latitud_suc'),
      "longitud_suc"=>$this->input->post('longitud_suc')
		);
    if($this->sucursal->insertar($datosNuevoSucursal)){
    $this->session->set_flashdata("confirmacion","Sucursal guardado exitosamente");
  }else {
    $this->session->set_flashdata("error","Error al guardar intente de nuevo");
  }
  redirect('sucursales/index');
}
public function borrar($id_suc){
  if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
    $this->session->set_flashdata("error","No tiene permisos para eliminar");
    redirect("sucursales/index");
  }
 if ($this->sucursal->borrar($id_suc)) { //invocando al modelo
   $this->session->set_flashdata("confirmacion","sucursal eliminado exitosamente");

 } else {
   $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
 }

      redirect('sucursales/index');

}
//fucion para renderizar firmularion de actualizacion
public function actualizar($id){
$data["sucursalEditar"]=$this->sucursal->obtenerPorId($id);
$this->load->view("header");
$this->load->view("sucursales/editar",$data);
$this->load->view("footer");}


public function procesarActualizacion(){
		$datosSucursalesEditado=array(
      "id_suc"=>$this->input->post('id_suc'),
      "nombre_suc"=>$this->input->post('nombre_suc'),
			"ciudad_suc"=>$this->input->post('ciudad_suc'),
  		"latitud_suc"=>$this->input->post('latitud_suc'),
      "longitud_suc"=>$this->input->post('longitud_suc')

		);
	 $id_suc=$this->input->post("id_suc");
		if($this->sucursal->actualizar($id_suc,$datosSucursalesEditado)){
	  $this->session->set_flashdata("confirmacion","Sucursales actualizada exitosamente");
		}else{
      $this->session->set_flashdata("error","Error al actualizar intente de nuevo");
		}
      redirect("sucursales/index");
	}


}//cerrar
