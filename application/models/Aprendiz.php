<?php
  class Aprendiz extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("aprendices_cef",$datos);
    }
    //Funcion pastringra consultar Instructores
    function obtenerTodos(){
      $listadoAprendices=
      $this->db->get("aprendices_cef");
      if($listadoAprendices->num_rows()>0){ //si hay datos
      return $listadoAprendices->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_apre_cef){
      $this->db->where("id_apre_cef",$id_apre_cef);
      if ($this->db->delete("aprendices_cef")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_apre_cef){
      $this->db->where("id_apre_cef",$id_apre_cef);
      $aprendiz=$this->db->get("aprendices_cef");
      if ($aprendiz->num_rows()>0) {
        return $aprendiz->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_apre_cef,$data){
      $this->db->where("id_apre_cef",$id_apre_cef);
      return $this->db->update('aprendices_cef',$data);
    }
  }//Cierre de la clase

 ?>
