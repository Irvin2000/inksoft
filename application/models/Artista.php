<?php
  class Artista extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("artistas_cef",$datos);
    }
    //Funcion pastringra consultar Instructores
    function obtenerTodos(){
      $listadoArtistas=
      $this->db->get("artistas_cef");
      if($listadoArtistas->num_rows()>0){ //si hay datos
      return $listadoArtistas->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_artista_cef){
      $this->db->where("id_artista_cef",$id_artista_cef);
      if ($this->db->delete("artistas_cef")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_artista_cef){
      $this->db->where("id_artista_cef",$id_artista_cef);
      $artista=$this->db->get("artistas_cef");
      if ($artista->num_rows()>0) {
        return $artista->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_artista_cef,$data){
      $this->db->where("id_artista_cef",$id_artista_cef);
      return $this->db->update('artistas_cef',$data);
    }
  }//Cierre de la clase

 ?>
