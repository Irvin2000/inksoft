<?php
  class Cita extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("citas_cef",$datos);
    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoCitas=
      $this->db->get("citas_cef");
      if($listadoCitas->num_rows()>0){ //si hay datos
      return $listadoCitas->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_cita_cef){
      $this->db->where("id_cita_cef",$id_cita_cef);
      if ($this->db->delete("citas_cef")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_cita_cef){
      $this->db->where("id_cita_cef",$id_cita_cef);
      $cita=$this->db->get("citas_cef");
      if ($cita->num_rows()>0) {
        return $cita->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_cita_cef,$data){
      $this->db->where("id_cita_cef",$id_cita_cef);
      return $this->db->update('citas_cef',$data);
    }
  }//Cierre de la clase

 ?>
