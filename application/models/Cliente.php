<?php
  class Cliente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("clientes_cef",$datos);
    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoClientes=
      $this->db->get("clientes_cef");
      if($listadoClientes->num_rows()>0){ //si hay datos
      return $listadoClientes->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_cli_cef){
      $this->db->where("id_cli_cef",$id_cli_cef);
      if ($this->db->delete("clientes_cef")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_cli_cef){
      $this->db->where("id_cli_cef",$id_cli_cef);
      $cliente=$this->db->get("clientes_cef");
      if ($cliente->num_rows()>0) {
        return $cliente->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_cli_cef,$datos){
      $this->db->where("id_cli_cef",$id_cli_cef);
      return $this->db->update('clientes_cef',$datos);
    }
  }//Cierre de la clase

 ?>
