<?php
  class Diseno extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("disenos_cef",$datos);
    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoDisenos=
      $this->db->get("disenos_cef");
      if($listadoDisenos->num_rows()>0){ //si hay datos
      return $listadoDisenos->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_diseno_cef){
      $this->db->where("id_diseno_cef",$id_diseno_cef);
      if ($this->db->delete("disenos_cef")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_diseno_cef){
      $this->db->where("id_diseno_cef",$id_diseno_cef);
      $diseno=$this->db->get("disenos_cef");
      if ($diseno->num_rows()>0) {
        return $diseno->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_diseno_cef,$datos){
      $this->db->where("id_diseno_cef",$id_diseno_cef);
      return $this->db->update('disenos_cef',$datos);
    }
  }//Cierre de la clase

 ?>
