<?php
  class producto extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("productos_cef",$datos);
    }
    //Funcion pastringra consultar Instructores
    function obtenerTodos(){
      $listadoProductos=
      $this->db->get("productos_cef");
      if($listadoProductos->num_rows()>0){ //si hay datos
      return $listadoProductos->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_pro_cef){
      $this->db->where("id_pro_cef",$id_pro_cef);
      if ($this->db->delete("productos_cef")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_pro_cef){
      $this->db->where("id_pro_cef",$id_pro_cef);
      $producto=$this->db->get("productos_cef");
      if ($producto->num_rows()>0) {
        return $producto->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_pro_cef,$data){
      $this->db->where("id_pro_cef",$id_pro_cef);
      return $this->db->update('productos_cef',$data);
    }
  }//Cierre de la clase

 ?>
