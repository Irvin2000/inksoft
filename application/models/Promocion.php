<?php
  class Promocion extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("promociones_cef",$datos);
    }
    //Funcion pastringra consultar Instructores
    function obtenerTodos(){
      $listadoPromociones=
      $this->db->get("promociones_cef");
      if($listadoPromociones->num_rows()>0){ //si hay datos
      return $listadoPromociones->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_pro_cef){
      $this->db->where("id_pro_cef",$id_pro_cef);
      if ($this->db->delete("promociones_cef")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_pro_cef){
      $this->db->where("id_pro_cef",$id_pro_cef);
      $promocion=$this->db->get("promociones_cef");
      if ($promocion->num_rows()>0) {
        return $promocion->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_pro_cef,$data){
      $this->db->where("id_pro_cef",$id_pro_cef);
      return $this->db->update('promociones_cef',$data);
    }
  }//Cierre de la clase

 ?>
