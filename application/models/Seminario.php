<?php
  class Seminario extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("seminario_cef",$datos);
    }
    //Funcion pastringra consultar Instructores
    function obtenerTodos(){
      $listadoSeminarios=
      $this->db->get("seminario_cef");
      if($listadoSeminarios->num_rows()>0){ //si hay datos
      return $listadoSeminarios->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_semi_cef){
      $this->db->where("id_semi_cef",$id_semi_cef);
      if ($this->db->delete("seminario_cef")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_semi_cef){
      $this->db->where("id_semi_cef",$id_semi_cef);
      $seminario=$this->db->get("seminario_cef");
      if ($seminario->num_rows()>0) {
        return $seminario->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_semi_cef,$data){
      $this->db->where("id_semi_cef",$id_semi_cef);
      return $this->db->update('seminario_cef',$data);
    }
  }//Cierre de la clase

 ?>
