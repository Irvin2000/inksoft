<?php
/**
 * Estudiante, ULTIMO PROCEDMIENTO
 */
class Sucursal extends CI_Model
{
  function __construct()
  {
    // code...
  		parent::__construct();
  }
  public function insertar($datos){
    //ponemos el nombre de la tabla de la BDD CREADO
    return $this->db->insert("sucursales_cef",$datos);
  }
  public function obtenerTodos(){
    $sucursal=$this->db->get("sucursales_cef");
    if ($sucursal->num_rows()>0) {
      return $sucursal;
    } else{
      return false;
    }

  }
  //funcion para eliminar un estudianteTemporal
  public function eliminarPorId($id){
    $this->db->where("id_suc",$id);
    return $this->db->delete("sucursales_cef");
  }

  public function obtenerPorId($id){
        $this->db->where("id_suc",$id);
        $sucursal=$this->db->get("sucursales_cef");
        if ($sucursal->num_rows()>0) {
          return $sucursal->row();
        } else {
          return false;
        }
      }

      //proceso de actualizaciom de estudientes
      public function actualizar($id,$datos){
        $this->db->where("id_suc",$id);
        return $this->db->update("sucursales_cef",$datos);
      }

}//cierre de la clase
