<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-account-group"></i><b> Aprendices</b> </h1>
  </div>
  <br>
</div>
<br>
<a href="<?php echo site_url('aprendices/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar aprendiz</b></a>
  <br>
<br>
<?php if ($aprendices): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_aprendices">
    <thead class="text-center">
      <tr class="text-center">
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">CEDULA</th>
        <th class="text-center">TELÉFONO</th>
        <th class="text-center">CORREO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach ($aprendices as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_apre_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_apre_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_apre_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_apre_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_apre_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->correo_apre_cef ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/aprendices/editar/<?php echo $filaTemporal->id_apre_cef; ?>" title="Editar Aprendiz"
            style="color:blue;">
              <button type="submit" name="button" class="btn btn-warning">
              <i class="mdi mdi-eyedropper"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
              <a href="<?php echo site_url(); ?>/aprendices/eliminar/<?php echo $filaTemporal->id_apre_cef; ?>" title="Eliminar aprendiz"
              onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
              style="color:red;">
                <button type="submit" name="button" class="btn btn-danger">
                <i class="mdi mdi-delete"></i>
                Eliminar
              </button>
              </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>
    <tfoot>
             <tr>
                 <th>Name</th>
                 <th>Position</th>
                 <th>Office</th>
                 <th>Age</th>
                 <th>Start date</th>
                 <th>Salary</th>
             </tr>
         </tfoot>

  </table>

<?php else: ?>
  <h1>No existen aprendices</h1>
<?php endif; ?>


<script type="text/javascript">
  $("#tbl_aprendices").DataTable();
</script>
