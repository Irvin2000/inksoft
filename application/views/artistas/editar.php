<h1 class="text-center" style="color:orange"><i class="mdi mdi-reload"></i><b> Actualizar tatuadores</b></h1>
<form class=""
id="frm_editar_artista"
action="<?php echo site_url('artistas/procesarActualizacion'); ?>"
method="post">
    <div class="row">
      <input type="hidden" name="id_artista_cef" id="id_artista_cef" value="<?php echo $artistaEditar->id_artista_cef; ?>">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          required
          name="nombre_artista_cef" value="<?php echo $artistaEditar->nombre_artista_cef; ?>"
          id="nombre_artista_cef">
      </div>
      <div class="col-md-4">
        <label for="">Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el apellido"
        class="form-control"
        name="apellido_artista_cef" value="<?php echo $artistaEditar->apellido_artista_cef; ?>"
        id="apellido_artista_cef">
      </div>
      <div class="col-md-4">
          <label for="">Cédula:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          required
          name="cedula_artista_cef" value="<?php echo $artistaEditar->cedula_artista_cef; ?>"
          id="cedula_artista_cef">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Teléfono:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese el número de teléfono"
          class="form-control"
          required
          name="telefono_artista_cef" value="<?php echo $artistaEditar->telefono_artista_cef; ?>"
          id="telefono_artista_cef">
      </div>
      <div class="col-md-4">
          <label for="">Correo:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="email"
          placeholder="Ingrese el correo"
          class="form-control"
          required
          name="correo_artista_cef" value="<?php echo $artistaEditar->correo_artista_cef; ?>"
          id="correo_artista_cef">
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="mdi mdi-reload"></i>
              Actualizar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/artistas/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_editar_artista").validate({
   rules:{
     cedula_artista_cef:{
       required:true,
       minlength:10,
       maxlength:10,
       digits:true
     },
     apellido_artista_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true

     },
     nombre_artista_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true
     },
     telefono_artista_cef:{
       required:true,
       minlength:10,
       maxlength:10
     },
     correo_artista_cef:{
       required:true,
       minlength:5,
       maxlength:50,
       email:true
     }
   },
   messages:{
     cedula_artista_cef:{
       required:"Por favor ingrese el número de cédula",
       minlength:"Cédula incorrecta, ingresa 10 digitos",
       maxlength:"Cédula incorrecta, ingresa 10 digitos",
       digits:"Este campo solo acepta números",
       number:"Este campo solo acepta números"
     },
     apellido_artista_cef:{
       required:"Por favor ingrese el primer apellido",
       minlength:"El apellido debe tener al menos 3 caracteres",
       maxlength:"Apellido incorrecto"
     },
     nombre_artista_cef:{
       required:"Por favor ingrese el nombre",
       minlength:"El nombre debe tener al menos 3 caracteres",
       maxlength:"Nombre incorrecto"
     },
     telefono_artista_cef:{
       required:"Por favor ingrese el teléfono",
       minlength:"El teléfono debe tener al menos 10 caracteres",
       maxlength:"Teléfono incorrecto"
     },
     correo_artista_cef:{
       required:"Por favor ingrese una dirección de correo",
       minlength:"La dirección debe tener al menos 5 caracteres",
       maxlength:"Dirección incorrecta",
email:"Esrciba un correo válido"
     },

   }
 });

 </script>
