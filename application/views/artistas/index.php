<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-palette-outline"></i><b> Tatuadores</b> </h1>
  </div>
  <br>
</div>
<a href="<?php echo site_url('artistas/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar tatuador</b></a>
  <br>
<br>
<?php if ($artistas): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_artistas">
    <thead class="text-center">
      <tr class="text-center">
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">CEDULA</th>
        <th class="text-center">TELÉFONO</th>
        <th class="text-center">CORREO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach ($artistas as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_artista_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_artista_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_artista_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_artista_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_artista_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->correo_artista_cef ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/artistas/editar/<?php echo $filaTemporal->id_artista_cef; ?>" title="Editar Artista"
            style="color:blue;">
              <button type="submit" name="button" class="btn btn-warning">
              <i class="mdi mdi-eyedropper"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
              <a href="<?php echo site_url(); ?>/artistas/eliminar/<?php echo $filaTemporal->id_artista_cef; ?>" title="Eliminar Artista"
              onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
              style="color:red;">
                <button type="submit" name="button" class="btn btn-danger">
                <i class="mdi mdi-delete"></i>
                Eliminar
              </button>
              </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <h1>No existen artistas</h1>
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_artistas").DataTable();
  $(document).ready(function() {
      $('#tbl_artistas').DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ]
      } );
  } );


</script>
