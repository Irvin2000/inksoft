<h1 class="text-center" style="color:green"><i class="mdi mdi-palette"></i> <b>Nuevo tatuador</b></h1>
<form class=""
id="frm_nuevo_artista"
action="<?php echo site_url(); ?>/artistas/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          required
          name="nombre_artista_cef" value=""
          id="nombre_artista_cef">
      </div>
      <div class="col-md-4">
          <label for=""> Apellido:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el  apellido"
          class="form-control"
          required
          name="apellido_artista_cef" value=""
          id="apellido_artista_cef">
      </div>
      <div class="col-md-4">
          <label for="">Cédula:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese el número de  cédula"
          class="form-control"
          required
          name="cedula_artista_cef" value=""
          id="cedula_artista_cef">
      </div>
    </div>
  <br>
    <div class="row">

      <div class="col-md-4">
        <label for="">Teléfono:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="number"
        placeholder="Ingrese el número de teléfono"
        class="form-control"
        required
        name="telefono_artista_cef" value=""
        id="telefono_artista_cef">
      </div>
      <div class="col-md-4">
          <label for="">Correo:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="email"
          placeholder="Ingrese el correo"
          class="form-control"
          required
          name="correo_artista_cef" value=""
          id="correo_artista_cef">
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="mdi mdi-content-save"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/artistas/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
$("#frm_nuevo_artista").validate({
  rules:{
    cedula_artista_cef:{
      required:true,
      minlength:10,
      maxlength:10,
      digits:true
    },
    apellido_artista_cef:{
      required:true,
      minlength:3,
      maxlength:50,
      letras:true

    },
    nombre_artista_cef:{
      required:true,
      minlength:3,
      maxlength:50,
      letras:true
    },
    telefono_artista_cef:{
      required:true,
      minlength:10,
      maxlength:10
    },
    correo_artista_cef:{
      required:true,
      minlength:5,
      maxlength:50,
      email:true
    }
  },
  messages:{
    cedula_artista_cef:{
      required:"Por favor ingrese el número de cédula",
      minlength:"Cédula incorrecta, ingresa 10 digitos",
      maxlength:"Cédula incorrecta, ingresa 10 digitos",
      digits:"Este campo solo acepta números",
      number:"Este campo solo acepta números"
    },
    apellido_artista_cef:{
      required:"Por favor ingrese el primer apellido",
      minlength:"El apellido debe tener al menos 3 caracteres",
      maxlength:"Apellido incorrecto"
    },
    nombre_artista_cef:{
      required:"Por favor ingrese el nombre",
      minlength:"El nombre debe tener al menos 3 caracteres",
      maxlength:"Nombre incorrecto"
    },
    telefono_artista_cef:{
      required:"Por favor ingrese el teléfono",
      minlength:"El teléfono debe tener al menos 10 caracteres",
      maxlength:"Teléfono incorrecto"
    },
    correo_artista_cef:{
      required:"Por favor ingrese una dirección de correo",
      minlength:"La dirección debe tener al menos 5 caracteres",
      maxlength:"Dirección incorrecta",
      email:"Esrciba un correo válido"

    },

  }
});

</script>
