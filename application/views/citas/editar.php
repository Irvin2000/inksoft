
<h1 class="text-center" style="color:orange"><i class="mdi mdi-reload"></i><b>Actualizar cita</b></h1>
<form class=""
id="frm_editar_cita"
action="<?php echo site_url('citas/procesarActualizacion'); ?>"
method="post">
    <div class="row">
      <input type="hidden" name="id_cita_cef" id="id_cita_cef" value="<?php echo $citaEditar->id_cita_cef; ?>">
      <div class="col-md-4">
          <label for="">Fecha:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="date"
          placeholder="Ingrese la fecha de la cita"
          class="form-control"
          required
          name="fecha_cita_cef" value="<?php echo $citaEditar->fecha_cita_cef; ?>"
          id="fecha_cita_cef">
      </div>
      <div class="col-md-4">
        <label for="">Hora:</label>
        <br>
        <input type="time"
        placeholder="Ingrese la hora de la cita"
        class="form-control"
        name="hora_cita_cef" value="<?php echo $citaEditar->hora_cita_cef; ?>"
        id="hora_cita_cef">
      </div>
        <div class="col-md-4">
            <label for="">Costo:
              <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="number"
            placeholder="Ingrese el costo de la cita"
            class="form-control"
            required
            name="costo_cita_cef" value="<?php echo $citaEditar->costo_cita_cef; ?>"
            id="costo_cita_cef">
        </div>
        <br><br>
        <div class="row">

        </div>

      <div class="col-md-4">
        <br>
          <label for="">Detalle:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el detalle de la cita"
          class="form-control"
          required
          name="detalle_cita_cef" value="<?php echo $citaEditar->detalle_cita_cef; ?>"
          id="detalle_cita_cef">
      </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="mdi mdi-reload"></i>
              Actualizar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/citas/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_editar_cita").validate({
   rules:{
     fecha_cita_cef:{
       required:true,
     },
     hora_cita_cef:{
       required:true,

     },
     detalle_cita_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true
     },
     costo_cita_cef:{
       required:true,
     }
   },
   messages:{
     fecha_cita_cef:{
       required:"Por favor ingrese la fecha de la cita",
     },
     hora_cita_cef:{
       required:"Por favor ingrese la hora de la cita",
     },
     detalle_cita_cef:{
       required:"Por favor ingrese el detalle de la cita",
       minlength:"Ingrese un detalle amplio",
       maxlength:"Ingrese un detalle mas corto"
     },
     costo_cita_cef:{
       required:"Por favor ingrese el costo de la cita",
     },

   }
 });

 </script>
