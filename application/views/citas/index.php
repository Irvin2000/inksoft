<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-calendar-check"></i> Citas Agendadas </h1>
  </div>
  <br>
</div>
<a href="<?php echo site_url('citas/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar cita</b></a>
  <br>
<br>
<?php if ($citas): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_citas">
    <thead>
      <tr class="text-center">
        <th class="text-center">ID</th>
        <th class="text-center">FECHA</th>
        <th class="text-center">HORA</th>
        <th class="text-center">DETALLE</th>
        <th class="text-center">COSTO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach ($citas as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_cita_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->fecha_cita_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->hora_cita_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->detalle_cita_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->costo_cita_cef ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/citas/editar/<?php echo $filaTemporal->id_cita_cef; ?>" title="Editar citas"
            style="color:blue;">
              <button type="submit" name="button" class="btn btn-warning">
              <i class="mdi mdi-eyedropper"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
              <a href="<?php echo site_url(); ?>/citas/eliminar/<?php echo $filaTemporal->id_cita_cef; ?>" title="Eliminar Cita"
              onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
              style="color:red;">
                <button type="submit" name="button" class="btn btn-danger">
                <i class="mdi mdi-delete"></i>
                Eliminar
              </button>
              </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <h1>No existen citas agendadas</h1>
<?php endif; ?>


<script type="text/javascript">
  $("#tbl_citas").DataTable();
</script>
