<h1 class="text-center" style="color:green"><i class="mdi mdi-calendar-clock"></i> <b>Agendar Cita</b></h1>
<form class=""
id="frm_nuevo_cita"
action="<?php echo site_url(); ?>/citas/guardar"
method="post">

  <div class="col-md-2">

  </div>
    <div class="row">
      <div class="col-md-4">
          <label for="">Fecha:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="date"
          placeholder="Ingrese la fecha de la cita"
          class="form-control"
          required
          name="fecha_cita_cef" value=""
          id="fecha_cita_cef">
      </div>
      <div class="col-md-4">
          <label for=""> Hora:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="time"
          placeholder="Ingrese la hora de la cita"
          class="form-control"
          required
          name="hora_cita_cef" value=""
          id="hora_cita_cef">
      </div>
      <div class="col-md-4">
        <label for="">Costo:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="number"
        placeholder="Ingrese el costo de la cita"
        class="form-control"
        required
        name="costo_cita_cef" value=""
        id="costo_cita_cef">
      </div>
    </div>

    <br>
    <div class="col-md-2">

    </div>
    <div class="row">


      <div class="col-md-4">
          <label for="">Detalle:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el detalle de la cita"
          class="form-control"
          required
          name="detalle_cita_cef" value=""
          id="detalle_cita_cef">
      </div>
    </div>

    <br>
    <div class="col-md-2">
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="mdi mdi-content-save"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/citas/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_cita").validate({
  rules:{
    fecha_cita_cef:{
      required:true,
    },
    hora_cita_cef:{
      required:true,

    },
    detalle_cita_cef:{
      required:true,
      minlength:3,
      maxlength:50,
      letras:true
    },
    costo_cita_cef:{
      required:true,
    }
  },
  messages:{
    fecha_cita_cef:{
      required:"Por favor ingrese la fecha de la cita",
    },
    hora_cita_cef:{
      required:"Por favor ingrese la hora de la cita",
    },
    detalle_cita_cef:{
      required:"Por favor ingrese el detalle de la cita",
      minlength:"Ingrese un detalle amplio",
      maxlength:"Ingrese un detalle mas corto"
    },
    costo_cita_cef:{
      required:"Por favor ingrese el costo de la cita",
    },

  }
});

</script>
