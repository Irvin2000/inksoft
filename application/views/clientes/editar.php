<h1 class="text-center" style="color:orange"><i class="mdi mdi-account-edit"></i> <b>Actualizar clientes</b></h1>
<form class=""
id="frm_editar_cliente"
action="<?php echo site_url('clientes/procesarActualizacion'); ?>"
method="post">
    <div class="row">
      <input type="hidden" name="id_cliente_cef" id="id_cliente_cef" value="<?php echo $clienteEditar->id_cli_cef; ?>">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          required
          name="nombre_cli_cef" value="<?php echo $clienteEditar->nombre_cli_cef; ?>"
          id="nombre_cli_cef">
      </div>
      <div class="col-md-4">
        <label for="">Apellido:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese el apellido"
        class="form-control"
        name="apellido_cli_cef" value="<?php echo $clienteEditar->apellido_cli_cef; ?>"
        id="apellido_cli_cef">
      </div>
      <div class="col-md-4">
          <label for="">Teléfono:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese el número de teléfono"
          class="form-control"
          required
          name="telefono_cli_cef" value="<?php echo $clienteEditar->telefono_cli_cef; ?>"
          id="telefono_cli_cef">
      </div>
    </div>
    <br>


      <div class="row">
      <div class="col-md-4">
          <label for="">Correo:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="email"
          placeholder="Ingrese el correo"
          class="form-control"
          required
          name="correo_cli_cef" value="<?php echo $clienteEditar->correo_cli_cef; ?>"
          id="correo_cli_cef">
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="mdi mdi-reload"></i>
              Actualizar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_editar_cliente").validate({
   rules:{

     nombre_cli_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true

     },
     apellido_cli_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true
     },
     telefono_cli_cef:{
       required:true,
       minlength:10,
       maxlength:10
     },
     correo_cli_cef:{
       required:true,
       minlength:5,
       maxlength:50,
       email:true

     }
   },
   messages:{
     nombre_cli_cef:{
       required:"Por favor ingrese el  nombre",
       minlength:"El apellido debe tener al menos 3 caracteres",
       maxlength:"Nombre incorrecto"
     },
     apellido_cli_cef:{
       required:"Por favor ingrese el apellido",
       minlength:"El apellido debe tener al menos 3 caracteres",
       maxlength:"Apellido incorrecto"
     },
     telefono_cli_cef:{
       required:"Por favor ingrese el teléfono",
       minlength:"El teléfono debe tener al menos 10 caracteres",
       maxlength:"Teléfono incorrecto"
     },
     correo_cli_cef:{
       required:"Por favor ingrese una dirección de correo",
       minlength:"La dirección debe tener al menos 5 caracteres",
       maxlength:"Dirección incorrecta",
       email:"Esrciba un correo válido"
     }

   }
 });

 </script>
