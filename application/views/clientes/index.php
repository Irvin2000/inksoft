<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-account-circle" ></i><b>Clientes</b> </h1>
  </div>
  <br>
</div>
<a href="<?php echo site_url('clientes/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar cliente</b></a>
  <br>
<br>
<?php if ($clientes): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_clientes">
    <thead>
      <tr class="text-center">
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">TELÉFONO</th>
        <th class="text-center">CORREO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach ($clientes as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_cli_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_cli_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_cli_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_cli_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->correo_cli_cef ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $filaTemporal->id_cli_cef; ?>" title="Editar Cliente"
            style="color:blue;">
              <button type="submit" name="button" class="btn btn-warning">
              <i class="mdi mdi-eyedropper"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
              <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli_cef; ?>" title="Eliminar cliente"
              onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
              style="color:red;">
                <button type="submit" name="button" class="btn btn-danger">
                <i class="mdi mdi-delete"></i>
                Eliminar
              </button>
              </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <h1>No existen Clientes</h1>
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_clientes").DataTable();
</script>
