<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-palette"></i><b>Diseños disponibles</b></h1>
  </div>
<br>
</div>
<a href="<?php echo site_url('disenos/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar diseño</b></a>
<br> <br>
<?php if ($disenos): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_disenos">
    <thead class="text-center">
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th> ANCHO </th>
        <th>LARGO </th>
        <th>COSTO</th>
        <th>ESTILO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach ($disenos as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_diseno_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_diseno_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->ancho_diseno_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->largo_diseno_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->costo_diseno_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->estilo_diseno_cef ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/disenos/editar/<?php echo $filaTemporal->id_diseno_cef; ?>" title="Editar diseno" title="Editar diseno"
            style="color:blue;">
              <button type="submit" name="button" class="btn btn-warning">
              <i class="mdi mdi-eyedropper"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
              <a href="<?php echo site_url(); ?>/disenos/eliminar/<?php echo $filaTemporal->id_diseno_cef; ?>" title="Eliminar diseño"
              onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
              style="color:red;">
                <button type="submit" name="button" class="btn btn-danger">
                <i class="mdi mdi-delete"></i>
                Eliminar
              </button>
              </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h1>No hay disenos</h1>
<?php endif; ?>


<script type="text/javascript">
  $("#tbl_disenos").DataTable();
</script>
