<h1 class="text-center" style="color:green"><i class="mdi mdi-palette"></i> <b>Nuevo diseño</b></h1>
<form class=""
id="frm_nuevo_diseno"
action="<?php echo site_url('disenos/guardar'); ?>"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          required
          name="nombre_diseno_cef" value=""
          id="nombre_diseno_cef">
      </div>
      <div class="col-md-4">
          <label for="">Ancho:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese el ancho del diseno"
          class="form-control"
          required
          name="ancho_diseno_cef" value=""
          id="ancho_diseno_cef">
      </div>
      <div class="col-md-4">
        <label for="">Largo:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="number"
        placeholder="Ingrese el largo del diseno"
        class="form-control"
        name="largo_diseno_cef" value=""
        id="largo_diseno_cef">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Costo:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el costo del diseno"
          class="form-control"
          required
          name="costo_diseno_cef" value=""
          id="costo_diseno_cef">
      </div>
      <div class="col-md-4">
          <label for="">Estilo:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el estilo del tatuaje"
          class="form-control"
          required
          name="estilo_diseno_cef" value=""
          id="estilo_diseno_cef">
      </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="mdi mdi-content-save"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/disenos/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_nuevo_diseno").validate({
   rules:{
     nombre_diseno_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true
     },
     ancho_diseno_cef:{
       required:true,
       minlength:1,
       maxlength:5,
       digits:true
     },
     largo_diseno_cef:{
       required:true,
       minlength:1,
       maxlength:5,
       digits:true
     },
     costo_diseno_cef:{
       required:true,
       minlength:1,
       maxlength:5,
       digits:true
     },
     estilo_diseno_cef:{
       required:true,
       minlength:5,
       maxlength:50,
       letras:true
     }
   },
   messages:{
     nombre_diseno_cef:{
       required:"Por favor ingrese el nombre",
       minlength:"El nombre debe tener al menos 3 caracteres",
       letras:"Este campo solo acepta letras"
     },
     ancho_diseno_cef:{
       required:"Por favor ingrese el ancho del tatuaje",
       minlength:"Ancho incorrecto",
       maxlength:"Ancho incorrecto",
       digits:"Este campo acepta numeros",

     },
     largo_diseno_cef:{
       required:"Por favor ingrese el largo del tatuaje",
       minlength:"Largo incorrecto",
       maxlength:"Largo incorrecto",
       digits:"Este campo acepta numeros",

     },
     costo_diseno_cef:{
       required:"Por favor ingrese el costo",
       minlength:"Costo incorrecto",
       digits:"Este campo acepta numeros",

     },
     estilo_diseno_cef:{
       required:"Por favor ingrese el estilo del tatuaje",
       minlength:"Estilo incorrecto",
       maxlength:"Estilo incorrecto",
       letras:"Este campo acepta letras"

     }

   }
 });

 </script>
