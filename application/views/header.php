
<?php if (!$this->session->userdata("conectado")): ?>
  <script type="text/javascript">
  window.location.href="<?php echo site_url('welcome/login'); ?>";
  </script>
<?php endif; ?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>InkSoft</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/mdi/css/materialdesignicons.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/css/vendor.bundle.base.css'); ?>">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/jvectormap/jquery-jvectormap.css') ;?>">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/flag-icon-css/css/flag-icon.min.css') ;?>">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/owl-carousel-2/owl.carousel.min.css') ;?>">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/vendors/owl-carousel-2/owl.theme.default.min.css') ;?>">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/style.css') ;?>">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="<?php echo base_url('plantilla/assets/images/faces/logo-mini1.png'); ?>" />

    <!-- IMPORTACION DE JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
  <!-- IMPORTACION DE JQUERY VALIDATE -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">



jQuery.validator.addMethod("letras", function (value, element){
return this.optional(element) || /^[A-Za-zÁÉÍÓÚÑáé íñóú ]*$/ .test(value);

},
"Este campo solo acepta letras");

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtaUfCa9wOGq3J8mJCDV9QyP9bcBezZbY&libraries=places&callback=initMap" >
            </script>
     <!-- //importacion de datatables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.6/css/buttons.dataTables.min.css"></script>

  </head>
  <body >
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar" style="background-image: url('<?php echo base_url(''); ?>plantilla/assets/images/faces/fondo.jpg');background-repeat: no-repeat; background-size:cover;">
        <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top" style="background-image: url('<?php echo base_url(''); ?>plantilla/assets/images/faces/inksoft.png');background-repeat: no-repeat; background-size:cover;">
          <!-- <a class="sidebar-brand brand-logo" ref="index.html"><img src="<?php echo base_url('plantilla/assets/images/faces/inksoft.png') ?>" alt="logo" /></a> -->
          <!-- <a class="sidebar-brand brand-logo-mini" href="index.html" style="background-image: url('')"><img src="<?php echo base_url('plantilla/assets/images/faces/logo-mini.png') ?>" alt="IS" /></a> -->
        </div>
        <ul class="nav">
          <li class="nav-item profile">
            <div class="profile-desc" >
              <div class="profile-pic">
                <div class="count-indicator">
                  <img class="img-xs rounded-circle " src="<?php echo base_url('plantilla/assets/images/faces/foto1.jpeg') ?>" alt="">
                  <span class="count bg-success"></span>
                </div>
                <div class="profile-name">
                  <h5 class="mb-0 font-weight-normal">Administrador</h5>
                  <span>Tatuador</span>
                </div>
              </div>
              <a href="#" id="profile-dropdown" data-toggle="dropdown"><i class="mdi mdi-dots-vertical"></i></a>
              <div class="dropdown-menu dropdown-menu-right sidebar-dropdown preview-list" aria-labelledby="profile-dropdown">
                <a href="#" class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-dark rounded-circle">
                      <i class="mdi mdi-settings text-primary"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <p class="preview-subject ellipsis mb-1 text-small">Configuraciones</p>
                  </div>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-dark rounded-circle">
                      <i class="mdi mdi-onepassword  text-info"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <p class="preview-subject ellipsis mb-1 text-small">Cambiar contraseña</p>
                  </div>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-dark rounded-circle">
                      <i class="mdi mdi-calendar-today text-success"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <p class="preview-subject ellipsis mb-1 text-small">Por hacer</p>
                  </div>
                </a>
              </div>
            </div>
          </li>
          <li class="nav-item nav-category">
            <span class="nav-link">Gestión</span>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" href="<?php echo base_url(); ?>">
              <span class="menu-icon">
                <i class="mdi mdi-laptop"></i>
              </span>
              <span class="menu-title">Inicio</span>
            </a>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <span class="menu-icon">
                <i class="mdi mdi-account"></i>
              </span>
              <span class="menu-title">Tatuadores</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('artistas/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('artistas/index'); ?>">Listado</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('plantilla/pages/ui-features/typography.html') ?>">Typography</a></li> -->
              </ul>
            </div>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#u" aria-expanded="false" aria-controls="u">
              <span class="menu-icon">
                <i class="mdi mdi-palette"></i>
              </span>
              <span class="menu-title">Diseños</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="u">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('disenos/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('disenos/index'); ?>">Listado</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('plantilla/pages/ui-features/typography.html') ?>">Typography</a></li> -->
              </ul>
            </div>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#ui" aria-expanded="false" aria-controls="ui">
              <span class="menu-icon">
                <i class="mdi mdi-account-circle"></i>
              </span>
              <span class="menu-title">Clientes</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('clientes/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('clientes/index'); ?>">Listado</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('plantilla/pages/ui-features/typography.html') ?>">Typography</a></li> -->
              </ul>
            </div>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#ui3" aria-expanded="false" aria-controls="ui3">
              <span class="menu-icon">
                <i class="mdi mdi-calendar"></i>
              </span>
              <span class="menu-title">Citas</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui3">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('citas/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('citas/index'); ?>">Listado</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('plantilla/pages/ui-features/typography.html') ?>">Typography</a></li> -->
              </ul>
            </div>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#ui4" aria-expanded="false" aria-controls="ui4">
              <span class="menu-icon">
                <i class="mdi mdi-fire"></i>
              </span>
              <span class="menu-title">Promociones</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui4">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('promociones/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('promociones/index'); ?>">Listado</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('plantilla/pages/ui-features/typography.html') ?>">Typography</a></li> -->
              </ul>
            </div>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#ui5" aria-expanded="false" aria-controls="ui5">
              <span class="menu-icon">
                <i class="mdi mdi-map-marker"></i>
              </span>
              <span class="menu-title">Sucursales</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui5">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('sucursales/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('sucursales/index'); ?>">Listado</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('plantilla/pages/ui-features/typography.html') ?>">Typography</a></li> -->
              </ul>
            </div>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#basic" aria-expanded="false" aria-controls="basic">
              <span class="menu-icon">
                <i class="mdi mdi-clipboard"></i>
              </span>
              <span class="menu-title">Supplies</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('productos/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('productos/index'); ?>">Listado</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('plantilla/pages/ui-features/typography.html') ?>">Typography</a></li> -->
              </ul>
            </div>
          </li>
          <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#basic3" aria-expanded="false" aria-controls="basic3">
              <span class="menu-icon">
                <i class="mdi mdi-school"></i>
              </span>
              <span class="menu-title">Seminarios</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic3">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('seminarios/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('seminarios/index'); ?>">Listado</a></li>
              </ul>
            </div>
          </li>
                <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#basic1" aria-expanded="false" aria-controls="basic1">
              <span class="menu-icon">
                <i class="mdi mdi-account-circle"></i>
              </span>
              <span class="menu-title">Aprendices</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic1">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('aprendices/nuevo'); ?>">Nuevo</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('aprendices/index'); ?>">Listado</a></li>

                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('plantilla/pages/ui-features/typography.html') ?>">Typography</a></li> -->
              </ul>
            </div>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar p-0 fixed-top d-flex flex-row" style="background-image: url('<?php echo base_url(''); ?>plantilla/assets/images/faces/fondo.jpg');background-repeat: no-repeat; background-size:cover;">
          <div class="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
            <!-- <a class="navbar-brand brand-logo-mini" href="index.html"><img src="assets/images/l" alt="hola" /></a> -->
          </div>
          <div class="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
              <span class="mdi mdi-menu"></span>
            </button>
            <ul class="navbar-nav w-100">
              <li class="nav-item w-100">
                <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search">
                  <input type="hidden" class="form-control" placeholder="Search products">
                </form>
              </li>
            </ul>
            <ul class="navbar-nav navbar-nav-right">
              <li class="nav-item dropdown d-none d-lg-block">
                <!-- <a class="nav-link btn btn-success create-new-button" id="createbuttonDropdown" data-toggle="dropdown" aria-expanded="false" href="#">+ Create New Project</a> -->
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="createbuttonDropdown">
                  <h6 class="p-3 mb-0">Projects</h6>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-file-outline text-primary"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Software Development</p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-web text-info"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">UI Development</p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-layers text-danger"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Software Testing</p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <p class="p-3 mb-0 text-center">See all projects</p>
                </div>
              </li>
              <!-- <li class="nav-item nav-settings d-none d-lg-block">
                <a class="nav-link" href="#">
                  <i class="mdi mdi-view-grid"></i>
                </a>
              </li> -->
              <li class="nav-item dropdown border-left">
                <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                  <i class="mdi mdi-email"></i>
                  <span class="count bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                  <h6 class="p-3 mb-0">Messages</h6>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face4.jpg" alt="image" class="rounded-circle profile-pic">
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Mark send you a message</p>
                      <p class="text-muted mb-0"> 1 Minutes ago </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face2.jpg" alt="image" class="rounded-circle profile-pic">
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Cregh send you a message</p>
                      <p class="text-muted mb-0"> 15 Minutes ago </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face3.jpg" alt="image" class="rounded-circle profile-pic">
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject ellipsis mb-1">Profile picture updated</p>
                      <p class="text-muted mb-0"> 18 Minutes ago </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <p class="p-3 mb-0 text-center">4 new messages</p>
                </div>
              </li>
              <li class="nav-item dropdown border-left">
                <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-bell"></i>
                  <span class="count bg-danger"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                  <h6 class="p-3 mb-0">Notifications</h6>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-calendar text-success"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1">Event today</p>
                      <p class="text-muted ellipsis mb-0"> Just a reminder that you have an event today </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-settings text-danger"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1">Settings</p>
                      <p class="text-muted ellipsis mb-0"> Update dashboard </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-link-variant text-warning"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1">Launch Admin</p>
                      <p class="text-muted ellipsis mb-0"> New admin wow! </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <p class="p-3 mb-0 text-center">See all notifications</p>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" id="profileDropdown" href="#" data-toggle="dropdown">
                  <div class="navbar-profile">
                    <img class="img-xs rounded-circle" src="<?php echo base_url(''); ?>plantilla/assets/images/faces/foto1.jpeg" alt="">
                    <p class="mb-0 d-none d-sm-block navbar-profile-name">
                      <?php echo $this->session->userdata("conectado")->email_usu; ?>

                    </p>
                    <i class="mdi mdi-menu-down d-none d-sm-block"></i>
                  </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="profileDropdown">
                  <h6 class="p-3 mb-0">Perfil</h6>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-settings text-success"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1">Configuración</p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-dark rounded-circle">
                        <i class="mdi mdi-logout text-danger"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <p class="preview-subject mb-1"
                      onclick="cerrarSistema();">Salir</p>
                      <script type="text/javascript">
        function cerrarSistema(){
          window.location.href="<?php echo site_url('welcome/cerrarSesion'); ?>";

        }
      </script>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <p class="p-3 mb-0 text-center">Configuración Avanzada</p>
                </div>
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
              <span class="mdi mdi-format-line-spacing"></span>
            </button>
          </div>
        </nav>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper" style="background-image: url('<?php echo base_url(''); ?>plantilla/assets/images/fondo123.jpg');background-repeat: no-repeat; background-size:cover; " >


    <!-- <nav class="navbar navbar-inverse"> -->
  <div class="container-fluid" >
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header" >
      <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button> -->
      <h2>
         <!-- <a class="navbar-brand" href="<?php echo base_url(); ?>"><i class="mdi mdi-cookie"></i> InkSoft</a> -->
</h2>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
      <!-- <  ul class="nav navbar-nav"> -->
        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li> -->
        <!-- <li class="dropdown">
          <a href="<?php echo site_url(); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ARTISTAS <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url(); ?>/artistas/nuevo">NUEVO</a></li>
            <li><a href="<?php echo site_url(); ?>/artistas/index">LISTADO</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="<?php echo site_url(); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CLIENTES <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url(); ?>/clientes/nuevo">NUEVO</a></li>
            <li><a href="<?php echo site_url(); ?>/clientes/index">LISTADO</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="<?php echo site_url(); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DISEÑOS <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url(); ?>/diseños/nuevo">NUEVO</a></li>
            <li><a href="<?php echo site_url(); ?>/diseños/index">LISTADO</a></li>

          </ul>
        </li> -->
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
