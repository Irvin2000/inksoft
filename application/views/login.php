
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- importacion del view port para que sea responsivo -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>InkSoft</title>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
      <!-- HH CTRL+SHIFT+7-->
      <!-- IMPORTACIÓN DE BOOTSTARP -->
      <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <!-- IMPORTACIÓN DE JQUERY VALIDATE -->
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/jquery.validate.min.js') ?>"> </script>
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/additional-methods.min.js') ?>"> </script>
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/messages_es_AR.min.js') ?>"> </script>
    <!-- IMPORTACIÓN DE DATATABLES -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"> </script>
    <!-- IMPORTACION DE SWEET ALERT V2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <?php if ($this->session->flashdata('conectado')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'CONFIRMACIÓN', //titulo
            '<?php echo $this->session->flashdata('conectado'); ?>', //Contenido o mensaje
            'success' //Tipo de alerta
          )
        });
      </script>
    <?php endif; ?>


    <?php if ($this->session->flashdata('error')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'ERROR', //titulo
            '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
            'error' //Tipo de alerta
          )
        });
      </script>
    <?php endif; ?>
 <!-- IMPORTACION DE BOOTSTARP SELECT -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" integrity="sha512-yDlE7vpGDP7o2eftkCiPZ+yuUyEcaBwoJoIhdXv71KZWugFqEphIS3PU60lEkFaz8RxaVsMpSvQxMBaKVwA5xg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/i18n/defaults-es_ES.min.js" integrity="sha512-RN/dgJo36dNkKVnb1XGzePP4/8XGa/r+On4XYUy8I1C5z+9SsIEU2rFh6TrunAnddKwtXwMdI0Se8HZxd0GtiQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  </head>
<body>
  <div class="row">
    <div class="col-md-4">

    </div>
    <div class="col-md-4 " style="background-image: url('<?php echo base_url(''); ?>plantilla/assets/images/login.jpg');background-repeat: repeat; background-size:cover;">
      <center> <h3 style="color:white"><i class="glyphicon glyphicon-lock"></i> <b>INICIO DE SESIÓN</b></h3> </center>
      <br>
      <br>
      <form id="frm_login" class="form-horizontal"  method="post" action="<?php echo site_url('welcome/iniciarSesion'); ?>">
            <fieldset>
              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="email_usu" style="color:white"><b>Email:</b></label>
                <div class="col-md-6">
                <input id="email_usu" name="email_usu" type="text" placeholder="Ingrese su email" class="form-control input-md" required="">
                <span class="help-block" style="color:white">Ej.juan@utc.com</span>
                </div>
              </div>

              <!-- Password input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="password_usu" style="color:white"><b>Contraseña:</b></label>
                <div class="col-md-6">

                  <input id="password_usu" name="password_usu" type="password" placeholder="Ingrese su contraseña" class="form-control input-md" required="">

                </div>
              </div>

              <!-- Button (Double) -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
                <div class="col-md-8">
                  <br>  
                  <button type="submit" class="btn btn-primary">Ingresar</button>
                  <button type="reset" class="btn btn-danger">Cancelar</button>
                </div>
              </div>

              </fieldset>
          </form>
    </div>
    <div class="col-md-4">

    </div>
  </div>

  <style>

  label.error{
      border:1px solid white;
      color:white;
      background-color:#E15B69;
      padding:5px;
      padding-left:15px;
      padding-right:15px;
      font-size:12px;
      opacity: 0;
        /visibility: hidden;/
        /position: absolute;/
        left: 0px;
        transform: translate(0, 10px);
        /background-color: white;/
        /padding: 1.5rem;/
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
        width: auto;
        margin-top:30px !important;


        z-index: 10;
        opacity: 1;
        visibility: visible;
        transform: translate(0, -20px);
        transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
        border-radius:10px;
        width:100%;

  }

  input.error{
      border:1px solid #E15B69;
  }


  select.error{
      border:1px solid #E15B69;
  }

  label.error:before{
       position: absolute;
        z-index: -1;
        content: "";
        right: calc(90% - 10px);
        top: -8px;
        border-style: solid;
        border-width: 0 10px 10px 10px;
        border-color: transparent transparent #E15B69 transparent;
        transition-duration: 0.3s;
        transition-property: transform;
  }


</style>
<!-- <script type="text/javascript">
  Swal.fire(
    'Good job!',
    'You clicked the button!',
    'success'
)
</script> -->
</body>
</html>



<!-- <h1>LOGIN</h1>
<form class=""
action="<?php echo site_url('welcome/iniciarSesion'); ?>"
method="post">
  <b>Email: </b>
  <input type="email" name="email_usu" value="" required>
  <br>
  <b>Contraseña:</b>
  <input type="password" name="password_usu" value="" required>
  <button type="submit" name="button">
	Ingresar
  </button>
</form> -->
