<h1 class="text-center" style="color:orange"><i class="mdi mdi-reload"></i> <b>Actualizar Productos</b></h1>
<form class=""
id="frm_editar_producto"
action="<?php echo site_url('productos/procesarActualizacion'); ?>"
method="post">
    <div class="row">
      <input type="hidden" name="id_pro_cef" id="id_pro_cef" value="<?php echo $productoEditar->id_pro_cef; ?>">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del producto"
          class="form-control"
          required
          name="nombre_pro_cef" value="<?php echo $productoEditar->nombre_pro_cef; ?>"
          id="nombre_pro_cef">
      </div>
      <div class="col-md-4">
        <label for="">Detalle:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese el detalle"
        class="form-control"
        name="detalle_pro_cef" value="<?php echo $productoEditar->detalle_pro_cef; ?>"
        id="detalle_pro_cef">
      </div>
      <div class="col-md-4">
        <label for="">Precio:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="number"
        placeholder="Ingrese el precio del producto"
        class="form-control"
        name="precio_pro_cef" value="<?php echo $productoEditar->precio_pro_cef; ?>"
        id="precio_pro_cef">

      </div>

    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="mdi mdi-reload"></i>
              Actualizar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/productos/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_editar_producto").validate({
   rules:{

     nombre_pro_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true

     },
     detalle_pro_cef:{
       required:true,
     },
     precio_pro_cef:{
       required:true,
       digits:true
     }
   },
   messages:{
     nombre_pro_cef:{
       required:"Por favor ingrese el nombre del producto",
       minlength:"Nombre invalido",
       maxlength:"Nombre invalido",
       letras:"Este campo solo acepta letras"
     },
     detalle_pro_cef:{
       required:"Por favor ingrese el detalle del producto",
     },
     precio_pro_cef:{
       required:"Por favor ingrese el precio del producto",
       digits:"Este campo solo acepta números"
     },

   }
 });

 </script>
