<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-clipboard-outline"></i> Productos Disponibles </h1>
  </div>
  <br>
</div>
<a href="<?php echo site_url('productos/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar Productos</b></a>
  <br>
<br>
<?php if ($productos): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_prodcutos">
    <thead>
      <tr class="text-center">
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">DETALLE</th>
        <th class="text-center">PRECIO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach ($productos as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_pro_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_pro_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->detalle_pro_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->precio_pro_cef ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/productos/editar/<?php echo $filaTemporal->id_pro_cef; ?>" title="Editar producto"
            style="color:blue;">
              <button type="submit" name="button" class="btn btn-warning">
              <i class="mdi mdi-eyedropper"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
              <a href="<?php echo site_url(); ?>/prodcutos/eliminar/<?php echo $filaTemporal->id_pro_cef; ?>" title="Eliminar productos"
              onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
              style="color:red;">
                <button type="submit" name="button" class="btn btn-danger">
                <i class="mdi mdi-delete"></i>
                Eliminar
              </button>
              </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <h1>No existen productos disponibles</h1>
<?php endif; ?>


<script type="text/javascript">
  $("#tbl_prodcutos").DataTable();
</script>
