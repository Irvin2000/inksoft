<h1 class="text-center" style="color:green"><i class="mdi mdi-clipboard"></i> <b>Productos</b></h1>
<form class=""
id="frm_nuevo_producto"
action="<?php echo site_url(); ?>/productos/guardar"
method="post">

  <div class="col-md-2">

  </div>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del producto"
          class="form-control"
          required
          name="nombre_pro_cef" value=""
          id="nombre_pro_cef">
      </div>
      <div class="col-md-4">
          <label for=""> Detalle:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el detalle del producto"
          class="form-control"
          required
          name="detalle_pro_cef" value=""
          id="detalle_pro_cef">
      </div>
      <div class="col-md-4">
        <label for="">Precio:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="number"
        placeholder="Ingrese el precio del producto"
        class="form-control"
        required
        name="precio_pro_cef" value=""
        id="precio_pro_cef">
      </div>

    </div>

    <br>
    <div class="col-md-2">
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="mdi mdi-content-save"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/productos/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_nuevo_producto").validate({
   rules:{

     nombre_pro_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true

     },
     detalle_pro_cef:{
       required:true,
     },
     precio_pro_cef:{
       required:true,
       digits:true
     }
   },
   messages:{
     nombre_pro_cef:{
       required:"Por favor ingrese el nombre del producto",
       minlength:"Nombre invalido",
       maxlength:"Nombre invalido",
       letras:"Este campo solo acepta letras"
     },
     detalle_pro_cef:{
       required:"Por favor ingrese el detalle del producto",
     },
     precio_pro_cef:{
       required:"Por favor ingrese el precio del producto",
       digits:"Este campo solo acepta números"
     },

   }
 });

 </script>
