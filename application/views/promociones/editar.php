<h1 class="text-center" style="color:orange"><i class="mdi mdi-reload"></i> <b>Actualizar Promociones</b></h1>
<form class=""
id="frm_editar_promocion"
action="<?php echo site_url('promociones/procesarActualizacion'); ?>"
method="post">
    <div class="row">
      <input type="hidden" name="id_pro_cef" id="id_pro_cef" value="<?php echo $promocionEditar->id_pro_cef; ?>">
      <div class="col-md-4">
          <label for="">Tipo de Promocion:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el tipo de promocion"
          class="form-control"
          required
          name="tipo_pro_cef" value="<?php echo $promocionEditar->tipo_pro_cef; ?>"
          id="tipo_pro_cef">
      </div>
      <div class="col-md-4">
        <label for="">Costo:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="number"
        placeholder="Ingrese el costo de la promoción"
        class="form-control"
        name="costo_pro_cef" value="<?php echo $promocionEditar->costo_pro_cef; ?>"
        id="costo_pro_cef">
      </div>
      <div class="col-md-4">
          <label for="">Fecha de Inicio:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="date"
          placeholder="Ingrese la fecha de inicio de la promoción"
          class="form-control"
          required
          name="fecha_inicio_pro_cef" value="<?php echo $promocionEditar->fecha_inicio_pro_cef; ?>"
          id="fecha_inicio_pro_cef">
      </div>
      <div class="col-md-4">
        <br>
          <label for="">Fecha Final:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="date"
          placeholder="Ingrese la fecha final de la promoción"
          class="form-control"
          required
          name="fecha_final_pro_cef" value="<?php echo $promocionEditar->fecha_final_pro_cef; ?>"
          id="fecha_final_pro_cef">
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="mdi mdi-reload"></i>
              Actualizar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/promociones/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_editar_promocion").validate({
   rules:{

     tipo_pro_cef:{
       required:true,
       minlength:3,
       maxlength:50,
  

     },
     costo_pro_cef:{
       required:true,
       digits:true
     },
     fecha_inicio_pro_cef:{
       required:true
     },
     fecha_final_pro_cef:{
       required:true

     }
   },
   messages:{
     tipo_pro_cef:{
       required:"Por favor ingrese el  tipo de la promoción",
       minlength:"La promoción debe tener al menos 3 caracteres",
       maxlength:"Promoción incorrecta"
     },
     costo_pro_cef:{
       required:"Por favor ingrese el costo de la promoción",
       digits:"Este campo solo acepta números"
     },
     fecha_inicio_pro_cef:{
       required:"Por favor ingrese la fecha inicial de la promoción"
     },
     fecha_final_pro_cef:{
       required:"Por favor ingrese la fecha final de la promoción"
     }

   }
 });

 </script>
