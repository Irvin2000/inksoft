<div class="row">
  <div class="col-md-12">

    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-tag"></i> Promociones Disponibles </h1>
  </div>
  <br>
</div>
<a href="<?php echo site_url('promociones/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar Promocion</b></a>
  <br>
<br>
<?php if ($promociones): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_promociones">
    <thead>
      <tr class="text-center">
        <th class="text-center">ID</th>
        <th class="text-center">TIPO</th>
        <th class="text-center">COSTO</th>
        <th class="text-center">FECHA DE INICIO</th>
        <th class="text-center">FECHA FINAL</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach ($promociones as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_pro_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->tipo_pro_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->costo_pro_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->fecha_inicio_pro_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->fecha_final_pro_cef ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/promociones/editar/<?php echo $filaTemporal->id_pro_cef; ?>" title="Editar promocion"
            style="color:blue;">
              <button type="submit" name="button" class="btn btn-warning">
              <i class="mdi mdi-eyedropper"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
              <a href="<?php echo site_url(); ?>/promociones/eliminar/<?php echo $filaTemporal->id_pro_cef; ?>" title="Eliminar promocion"
              onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
              style="color:red;">
                <button type="submit" name="button" class="btn btn-danger">
                <i class="mdi mdi-delete"></i>
                Eliminar
              </button>
              </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <h1>No existen promociones disponibles</h1>
<?php endif; ?>


<script type="text/javascript">
  $("#tbl_promociones").DataTable();
</script>
