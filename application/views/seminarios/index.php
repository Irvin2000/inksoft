<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-check-decagram"></i> Seminarios Disponibles </h1>
  </div>
  <br>
</div>
<a href="<?php echo site_url('seminarios/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar Seminario</b></a>
  <br>
<br>
<?php if ($seminarios): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_seminarios">
    <thead>
      <tr class="text-center">
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TEMA</th>
        <th class="text-center">DETALLE</th>
        <th class="text-center">HORAS</th>
        <th class="text-center">FECHA</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach ($seminarios as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_semi_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_semi_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->tema_semi_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->detalle_semi_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->horas_semi_cef ?>
          </td>
          <td>
            <?php echo $filaTemporal->fecha_semi_cef ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/seminarios/editar/<?php echo $filaTemporal->id_semi_cef; ?>" title="Editar seminario"
            style="color:blue;">
              <button type="submit" name="button" class="btn btn-warning">
              <i class="mdi mdi-eyedropper"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
              <a href="<?php echo site_url(); ?>/seminarios/eliminar/<?php echo $filaTemporal->id_semi_cef; ?>" title="Eliminar seminario"
              onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
              style="color:red;">
                <button type="submit" name="button" class="btn btn-danger">
                <i class="mdi mdi-delete"></i>
                Eliminar
              </button>
              </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <h1>No existen seminarios disponibles</h1>
<?php endif; ?>


<script type="text/javascript">
  $("#tbl_seminarios").DataTable();
</script>
