<h1 class="text-center" style="color:green"><i class="mdi mdi-calendar-clock"></i> <b>Agendar seminario</b></h1>
<form class=""
id="frm_nuevo_seminario"
action="<?php echo site_url(); ?>/seminarios/guardar"
method="post">

  <div class="col-md-2">

  </div>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del seminario"
          class="form-control"
          required
          name="nombre_semi_cef" value=""
          id="nombre_semi_cef">
      </div>
      <div class="col-md-4">
          <label for=""> Tema:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el tema del seminario"
          class="form-control"
          required
          name="tema_semi_cef" value=""
          id="tema_semi_cef">
      </div>
      <div class="col-md-4">
        <label for="">Detalle:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="text"
        placeholder="Ingrese un detalle del seminario"
        class="form-control"
        required
        name="detalle_semi_cef" value=""
        id="detalle_semi_cef">
      </div>


      <div class="col-md-4">
        <br>
          <label for="">Horas:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese las horas totales del seminario"
          class="form-control"
          required
          name="horas_semi_cef" value=""
          id="horas_semi_cef">
      </div>
      <div class="col-md-4">
        <br>
          <label for="">Fecha:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="date"
          placeholder="Ingrese la fecha del seminario"
          class="form-control"
          required
          name="fecha_semi_cef" value=""
          id="fecha_semi_cef">
      </div>
    </div>

    <br>
    <div class="col-md-2">
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="mdi mdi-content-save"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/seminarios/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_nuevo_seminario").validate({
   rules:{
     nombre_semi_cef:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true
     },
     tema_semi_cef:{
       required:true,
       letras:true
     },
     detalle_semi_cef:{
       required:true
     },
     horas_semi_cef:{
       required:true,
       digits:true,
       minlength:1,
       maxlength:3
     },
     fecha_semi_cef:{
       required:true
     }
   },
   messages:{
     nombre_semi_cef:{
       required:"Por favor ingrese el nombre del seminario",
       minlength:"El seminario debe tener al menos 3 caracteres",
       letras:"Este campo solo acepta letras"
     },
     tema_semi_cef:{
       required:"Por favor ingrese el tema del seminario",
       letras:"Este campo solo acepta letras"
     },
     detalle_semi_cef:{
       required:"Por favor ingrese el detalle del seminario",
     },
     horas_semi_cef:{
       required:"Por favor ingrese las horas totales del seminario",
       digits:"Este campo solo acepta números",
       minlength:"Ingrese al menos 1 número",
       maxlength:"Este campo solo acepta 3 números"
     },
     fecha_semi_cef:{
       required:"Por favor ingrese la fecha del seminario",
     },

   }
 });

 </script>
