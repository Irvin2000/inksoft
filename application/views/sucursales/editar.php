<h1 class="text-center" style="color:orange"><i class="mdi mdi-reload"></i> <b>Actualizar Sucursales</b></h1>
<form class=""
id="frm_editar_sucursal"
action="<?php echo site_url('sucursales/procesarActualizacion'); ?>"
method="post">
    <div class="row">
      <input type="hidden" name="id_suc" id="id_suc" value="<?php echo $sucursalEditar->id_suc; ?>">
      <div class="col-md-4">
        <label for="">Nombre:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre de la sucursal"
        class="form-control"
        name="nombre_suc" value="<?php echo $sucursalEditar->nombre_suc; ?>"
        id="nombre_suc">
      </div>
      <div class="col-md-4">
          <label for="">Ciudad:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese la ciudad de la sucursal"
          class="form-control"
          required
          name="ciudad_suc" value="<?php echo $sucursalEditar->ciudad_suc; ?>"
          id="ciudad_suc">
      </div>
      <div class="col-md-4">

          <label for="">Latitud:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese la latitud"
          class="form-control"
          required
          name="latitud_suc" value="<?php echo $sucursalEditar->latitud_suc; ?>"
          id="latitud_suc">
      </div>
      <div class="col-md-4">
        <br>
          <label for="">Longitud:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese la longitud"
          class="form-control"
          required
          name="longitud_suc" value="<?php echo $sucursalEditar->longitud_suc; ?>"
          id="longitud_suc">
      </div>
    </div>
    <div class="col-md-12">
      <center>
        <br>
            <h4>Arrastre el marcador hacia la nueva sucursal (con esto se agregara latitud y longitud)</h4>
            <div id="mapa" style="height: 300px; width:100%; border:2px solid black;"></div>
            <br><br>
            </center>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="mdi mdi-reload"></i>
              Actualizar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/sucursales/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
    function initMap(){
      // alert("API IMPORTADO EXITOSAMENTE");
      //Definiendo una coordenada
      var latitud_longitud =new google.maps.LatLng(-0.19620656131253247, -78.46008063558395);




      //creando el mapa
      var mapa=new google.maps.Map(
        document.getElementById('mapa'),
        {
          center:latitud_longitud,
          zoom:17,
          mapTypeId:google.maps.MapTypeId.ROADMAP
        }
      );
      //creando un marcador de mi domicilio
      var marcador = new google.maps.Marker({
        position: latitud_longitud,
        map:mapa,
        title:"Seleccione la direccion ",
        draggable:true
      });

      google.maps.event.addListener(
        marcador,
        'dragend',
        function(event){
          var latitud=this.getPosition().lat();
          var longitud=this.getPosition().lng();
          // alert("LATITUD:"+latitud);
          // alert("LONGITUD:"+longitud);
          document.getElementById("latitud_suc").value=latitud;
          document.getElementById("longitud_suc").value=longitud;
        }
      )




    }
</script>

<script type="text/javascript">
$("#frm_editar_sucursal").validate({
  rules:{

    nombre_suc:{
      required:true,
      minlength:3,
      maxlength:50,

    },
    ciudad_suc:{
      required:true,
    },
    latitud_suc:{
      required:true,
    },
    longitud_suc:{
      required:true,
    }
  },
  messages:{
    nombre_suc:{
      required:"Por favor ingrese el  nombre de la sucursal",
      minlength:"La sucursal debe tener al menos 3 caracteres",
      maxlength:"Nombre incorrecto"
    },
    ciudad_suc:{
      required:"Por favor ingrese la ciudad de la sucursal",
      minlength:"La Ciudad debe tener al menos 3 caracteres",
      digits:"Nombre incorrecto"
    },
    latitud_suc:{
      required:"Por favor ingrese la latitud",
    },
    longitud_suc:{
      required:"Por favor ingrese la longitud",
    }

  }
});

</script>
