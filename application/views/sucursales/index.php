<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:lightblue"><i class="mdi mdi-map-marker-radius"></i> Sucursales </h1>
  </div>
  <br>
</div>
<a href="<?php echo site_url('sucursales/nuevo'); ?>" class="btn btn-primary">
  <i class="mdi mdi-plus-circle"></i><b>Agregar Sucursal</b></a>
  <br>
<br>
<?php if ($listadoSucursales): ?>
    <table class="table table-striped ; table-bordered table-hover" id="tbl_sucursales">
        <thead>
           <tr style="background-color: #2FD2AF">
             <th class="text-center">ID</th>
             <th class="text-center">NOMBRE</th>
             <th class="text-center">CIUDAD</th>
             <th class="text-center">LATITUD</th>
            <th class="text-center">LONGITUD</th>
            <th class="text-center">UBICACIÓN</th>
            <th class="text-center">ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoSucursales->result() as $sucursaltemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $sucursaltemporal->id_suc; ?>
              </td>
              <td class="text-center">
                <?php echo $sucursaltemporal->nombre_suc; ?>
              </td>
              <td class="text-center">
                <?php echo $sucursaltemporal->ciudad_suc; ?>
              </td>
              <td class="text-center">
                <?php echo $sucursaltemporal->latitud_suc; ?>
              </td>
              <td class="text-center">
                <?php echo $sucursaltemporal->longitud_suc; ?>
              </td>
              <td class="">

                  <div id="mapa" style="height:150px; width:240px; border:4px solid #2FD2AF;"></div>

              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/sucursales/actualizar/<?php echo $sucursaltemporal->id_suc; ?>" title="Editar sucursal"
                style="color:blue;">
                  <button type="submit" name="button" class="btn btn-warning">
                  <i class="mdi mdi-eyedropper"></i>
                       Editar
                </button>
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
                  <a href="<?php echo site_url(); ?>/sucursales/eliminar/<?php echo $filaTemporal->id_suc; ?>" title="Eliminar sucursal"
                  onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
                  style="color:red;">
                    <button type="submit" name="button" class="btn btn-danger">
                    <i class="mdi mdi-delete"></i>
                    Eliminar
                  </button>
                  </a>
                <?php endif; ?>
              </td>
              </tr>
          <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1>No hay sucursales</h1>
<?php endif; ?>





<div class="row">

  <div class="col-md-2"></div>


  <script type="text/javascript">

        function initMap(){
          // alert("API OK")
          latitud_longitud = new google.maps.LatLng(<?php echo $sucursaltemporal->latitud_suc; ?>,
            <?php echo $sucursaltemporal->longitud_suc ?>);

          var mapa=new google.maps.Map(
            document.getElementById("mapa"),
            {
              center:latitud_longitud,
              zoom:18,
              mapTypeId:google.maps.MapTypeId.ROADMAP
            }
          )

          <?php if ($listadoSucursales): ?>
              <?php foreach ($listadoSucursales->result() as $sucursaltemporal): ?>
                   latitud_longitud1=new google.maps.LatLng(<?php echo $sucursaltemporal->latitud_suc; ?>,
                     <?php echo $sucursaltemporal->longitud_suc ?>);

                       console.log(latitud_longitud)

                        //creando otro marcador
                            var marcador = new google.maps.Marker({
                              position: latitud_longitud1,
                              map:mapa,
                              title:"  <?php echo $sucursaltemporal->nombre_suc; ?>",
                                icon:{url:'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'}
                              // icon:"https://img.freepik.com/vector-gratis/ilustracion-simbolo-ubicacion-aislado-fondo_53876-6347.jpg?w=2000"
                            });

                // console.log(latitud_longitud);
              <?php endforeach; ?>
          <?php endif; ?>

        }
      </script>

</div>

<script type="text/javascript">
  $("#tbl_sucursales").DataTable();
</script>
