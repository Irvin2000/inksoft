
<h1 class="text-center" style="color:green"> <i class="mdi mdi-home-map-marker"></i><b>Agregar sucursales</b> </h1>
<form class=""
id="frm_nueva_sucursal"
action="<?php echo site_url('sucursales/guardarSucursales') ?>" method="post">

  <div class="row">

    <div class="col-md-4">
      <label for="">Nombre:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
    <br>
      <input type="text" required name="nombre_suc"
      class="form-control" placeholder="Ingrese nombre del estudio" value=""
     id="nombre_suc">
    </div>
    <div class="col-md-4">
      <label for=""> Ciudad:
        <span class="obligatorio">(Obligatorio)</span>
      </label>
      <input type="text" required name="ciudad_suc"
      class="form-control" placeholder="Ingrese la ciudad" value="" required
       id="ciudad_suc">
    </div>
    <div class="col-md-4">
      <label for=""> Latitud:
        <span class="obligatorio">(Obligatorio)</span>
      </label>
      <input type="text" required name="latitud_suc" id="latitud_suc"
      class="form-control" placeholder="Ingrese la latitud" value="">
    </div>
    <div class="col-md-4">
      <br>
      <label for=""> Longitud:
        <span class="obligatorio">(Obligatorio)</span>
      </label>
      <input type="text" required name="longitud_suc" id="longitud_suc"
      class="form-control" placeholder="Ingrese la longitud" value="">
    </div>

</div>
<br>




<div class="row">

<!-- <div class="col-md-12"  >
<div class="col-md-6">
<div class="row">
  <br>
  <h3>COORDENADA 1</h3>
  <div class="col-md-6">
    <b>LAT:</b><br>
    <input type="number" required name="latitud_suc"
    class="form-control" placeholder="Ingrese latitud" value=""
    id="latitud_suc" readonly>
  </div>
  <div class="col-md-6">
    <b>LON:</b><br>
    <input type="number" required name="longitud_suc"
    class="form-control" placeholder="Ingrese altitud" value=""
    id="longitud_suc" readonly>
  </div></div>
  <br><br><br><br>  <br><br><br><br>

</div> -->



  <div class="col-md-12">
    <center>
          <h4>Arrastre el marcador hacia la nueva sucursal (con esto se agregara latitud y longitud)</h4>
          <div id="mapa" style="height: 300px; width:100%; border:2px solid black;"></div>
          <br><br>
          </center>
  </div>

  <br>
  <div class="col-md-5">
  </div>
  <br>
  <div class="row">
      <div class="col-md-12 text-center">
          <button type="submit" name="button"
          class="btn btn-primary">
          <i class="mdi mdi-content-save"></i>
            Guardar
          </button>
          &nbsp;
          <a href="<?php echo site_url(); ?>/sucursales/index"
            class="btn btn-danger">
            <i class="mdi mdi-close-octagon"></i>
            Cancelar
          </a>
      </div>
  </div>


  <script type="text/javascript">
      function initMap(){
        // alert("API IMPORTADO EXITOSAMENTE");
        //Definiendo una coordenada
        var latitud_longitud =new google.maps.LatLng(-0.19620656131253247, -78.46008063558395);




        //creando el mapa
        var mapa=new google.maps.Map(
          document.getElementById('mapa'),
          {
            center:latitud_longitud,
            zoom:17,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        //creando un marcador de mi domicilio
        var marcador = new google.maps.Marker({
          position: latitud_longitud,
          map:mapa,
          title:"Seleccione la direccion ",
          draggable:true
        });

        google.maps.event.addListener(
          marcador,
          'dragend',
          function(event){
            var latitud=this.getPosition().lat();
            var longitud=this.getPosition().lng();
            // alert("LATITUD:"+latitud);
            // alert("LONGITUD:"+longitud);
            document.getElementById("latitud_suc").value=latitud;
            document.getElementById("longitud_suc").value=longitud;
          }
        )




      }
  </script>

</div>

</div>

<br>








<br>


</form>

<script type="text/javascript">
$("#frm_nueva_sucursal").validate({
  rules:{

    nombre_suc:{
      required:true,
      minlength:3,
      maxlength:50,

    },
    ciudad_suc:{
      required:true,
    },
    latitud_suc:{
      required:true
    },
    longitud_suc:{
      required:true
        }
  },
  messages:{
    nombre_suc:{
      required:"Por favor ingrese el  nombre de la sucursal",
      minlength:"La sucursal debe tener al menos 3 caracteres",
      maxlength:"Nombre incorrecto"
    },
    ciudad_suc:{
      required:"Por favor ingrese la ciudad de la sucursal",
      minlength:"La Ciudad debe tener al menos 3 caracteres"
        },
    latitud_suc:{
      required:"Por favor ingrese la latitud"
    },
    longitud_suc:{
      required:"Por favor ingrese la longitud"
        }

  }
});

</script>
