-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-06-2023 a las 15:15:36
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inksoft`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendices_cef`
--

CREATE TABLE `aprendices_cef` (
  `id_apre_cef` int(11) NOT NULL,
  `nombre_apre_cef` varchar(100) NOT NULL,
  `apellido_apre_cef` varchar(100) NOT NULL,
  `cedula_apre_cef` varchar(11) NOT NULL,
  `telefono_apre_cef` varchar(11) NOT NULL,
  `correo_apre_cef` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `artistas_cef`
--

CREATE TABLE `artistas_cef` (
  `id_artista_cef` int(11) NOT NULL,
  `nombre_artista_cef` varchar(100) NOT NULL,
  `apellido_artista_cef` varchar(100) NOT NULL,
  `cedula_artista_cef` varchar(10) NOT NULL,
  `telefono_artista_cef` varchar(10) NOT NULL,
  `correo_artista_cef` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `artistas_cef`
--

INSERT INTO `artistas_cef` (`id_artista_cef`, `nombre_artista_cef`, `apellido_artista_cef`, `cedula_artista_cef`, `telefono_artista_cef`, `correo_artista_cef`) VALUES
(1, 'Irvin', 'Cadena', '1753560182', '0998815499', 'irvin.cadena0182@utc.edu.ec');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas_cef`
--

CREATE TABLE `citas_cef` (
  `id_cita_cef` int(11) NOT NULL,
  `fecha_cita_cef` date NOT NULL,
  `hora_cita_cef` time NOT NULL,
  `detalle_cita_cef` varchar(100) NOT NULL,
  `costo_cita_cef` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes_cef`
--

CREATE TABLE `clientes_cef` (
  `id_cli_cef` int(11) NOT NULL,
  `nombre_cli_cef` varchar(100) NOT NULL,
  `apellido_cli_cef` varchar(100) NOT NULL,
  `telefono_cli_cef` varchar(10) NOT NULL,
  `correo_cli_cef` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disenos_cef`
--

CREATE TABLE `disenos_cef` (
  `id_diseno_cef` int(11) NOT NULL,
  `nombre_diseno_cef` varchar(100) NOT NULL,
  `ancho_diseno_cef` int(11) NOT NULL,
  `largo_diseno_cef` int(11) NOT NULL,
  `costo_diseno_cef` int(11) NOT NULL,
  `estilo_diseno_cef` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `disenos_cef`
--

INSERT INTO `disenos_cef` (`id_diseno_cef`, `nombre_diseno_cef`, `ancho_diseno_cef`, `largo_diseno_cef`, `costo_diseno_cef`, `estilo_diseno_cef`) VALUES
(2, 'Rogelio', 12, 12, 12, 'Puntillismo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_cef`
--

CREATE TABLE `productos_cef` (
  `id_pro_cef` int(11) NOT NULL,
  `nombre_pro_cef` varchar(100) NOT NULL,
  `detalle_pro_cef` varchar(100) NOT NULL,
  `precio_pro_cef` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones_cef`
--

CREATE TABLE `promociones_cef` (
  `id_pro_cef` int(11) NOT NULL,
  `tipo_pro_cef` varchar(50) NOT NULL,
  `costo_pro_cef` varchar(50) NOT NULL,
  `fecha_inicio_pro_cef` date NOT NULL,
  `fecha_final_pro_cef` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seminario_cef`
--

CREATE TABLE `seminario_cef` (
  `id_semi_cef` int(11) NOT NULL,
  `nombre_semi_cef` varchar(100) NOT NULL,
  `tema_semi_cef` varchar(100) NOT NULL,
  `detalle_semi_cef` varchar(100) NOT NULL,
  `horas_semi_cef` varchar(100) NOT NULL,
  `fecha_semi_cef` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales_cef`
--

CREATE TABLE `sucursales_cef` (
  `id_suc` int(11) NOT NULL,
  `nombre_suc` varchar(100) NOT NULL,
  `ciudad_suc` varchar(100) NOT NULL,
  `latitud_suc` varchar(100) NOT NULL,
  `longitud_suc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sucursales_cef`
--

INSERT INTO `sucursales_cef` (`id_suc`, `nombre_suc`, `ciudad_suc`, `latitud_suc`, `longitud_suc`) VALUES
(1, 'TATTOO ART LA MARIN', 'MADRID', '-0.19512295518914313', '-78.45942617658432');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_cef`
--

CREATE TABLE `usuario_cef` (
  `id_usu` int(11) NOT NULL,
  `nombre_usu` varchar(150) DEFAULT NULL,
  `apellido_usu` varchar(150) DEFAULT NULL,
  `email_usu` varchar(150) DEFAULT NULL,
  `password_usu` varchar(500) DEFAULT NULL,
  `perfil_usu` varchar(150) DEFAULT 'SECRETARIO',
  `creacion_usu` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_cef`
--

INSERT INTO `usuario_cef` (`id_usu`, `nombre_usu`, `apellido_usu`, `email_usu`, `password_usu`, `perfil_usu`, `creacion_usu`) VALUES
(1, 'Irvin', 'Cadena', 'irvin@utc.com', '12345', 'ADMINISTRADOR', '2023-06-26 19:23:47'),
(2, 'Natalia', 'Espin', 'natalia@utc.com', '12345', 'ADMINISTRADOR', '2023-06-26 19:23:47'),
(3, 'Luis', 'Farias', 'luis@utc.com', '12345', 'ADMINISTRADOR', '2023-06-26 19:24:37');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aprendices_cef`
--
ALTER TABLE `aprendices_cef`
  ADD PRIMARY KEY (`id_apre_cef`);

--
-- Indices de la tabla `artistas_cef`
--
ALTER TABLE `artistas_cef`
  ADD PRIMARY KEY (`id_artista_cef`);

--
-- Indices de la tabla `citas_cef`
--
ALTER TABLE `citas_cef`
  ADD PRIMARY KEY (`id_cita_cef`);

--
-- Indices de la tabla `clientes_cef`
--
ALTER TABLE `clientes_cef`
  ADD PRIMARY KEY (`id_cli_cef`);

--
-- Indices de la tabla `disenos_cef`
--
ALTER TABLE `disenos_cef`
  ADD PRIMARY KEY (`id_diseno_cef`);

--
-- Indices de la tabla `productos_cef`
--
ALTER TABLE `productos_cef`
  ADD PRIMARY KEY (`id_pro_cef`);

--
-- Indices de la tabla `promociones_cef`
--
ALTER TABLE `promociones_cef`
  ADD PRIMARY KEY (`id_pro_cef`);

--
-- Indices de la tabla `seminario_cef`
--
ALTER TABLE `seminario_cef`
  ADD PRIMARY KEY (`id_semi_cef`);

--
-- Indices de la tabla `sucursales_cef`
--
ALTER TABLE `sucursales_cef`
  ADD PRIMARY KEY (`id_suc`);

--
-- Indices de la tabla `usuario_cef`
--
ALTER TABLE `usuario_cef`
  ADD PRIMARY KEY (`id_usu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aprendices_cef`
--
ALTER TABLE `aprendices_cef`
  MODIFY `id_apre_cef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `artistas_cef`
--
ALTER TABLE `artistas_cef`
  MODIFY `id_artista_cef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `citas_cef`
--
ALTER TABLE `citas_cef`
  MODIFY `id_cita_cef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `clientes_cef`
--
ALTER TABLE `clientes_cef`
  MODIFY `id_cli_cef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `disenos_cef`
--
ALTER TABLE `disenos_cef`
  MODIFY `id_diseno_cef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `productos_cef`
--
ALTER TABLE `productos_cef`
  MODIFY `id_pro_cef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `promociones_cef`
--
ALTER TABLE `promociones_cef`
  MODIFY `id_pro_cef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `seminario_cef`
--
ALTER TABLE `seminario_cef`
  MODIFY `id_semi_cef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sucursales_cef`
--
ALTER TABLE `sucursales_cef`
  MODIFY `id_suc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario_cef`
--
ALTER TABLE `usuario_cef`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
